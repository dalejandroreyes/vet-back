package com.vet.veterinaria.services.interfaces;

import com.vet.veterinaria.models.Pet;

public interface IPetService extends IGenericService<Pet, Long>{
}
