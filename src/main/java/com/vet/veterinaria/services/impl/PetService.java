package com.vet.veterinaria.services.impl;

import com.vet.veterinaria.models.Pet;
import com.vet.veterinaria.repositories.PetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PetService {

    private PetRepository petRepository;

    @Autowired
    public PetService(PetRepository petRepository) {
        this.petRepository = petRepository;
    }
    public Pet create(Pet pet){
        return petRepository.save(pet);
    }

    public Pet update(Pet pet){
        return petRepository.save(pet);
    }

    public List<Pet> getAllPets(){
        return petRepository.findAll();
    }

    public Optional<Pet> findById(Long id){
        return petRepository.findById(id);
    }

    public Object deleteById(Long id){
        petRepository.deleteById(id);
        return null;
    }

}