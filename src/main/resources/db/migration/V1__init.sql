-- auto-generated definition
create table tb_users
(
    us_id        bigserial not null
        constraint tb_users_pkey
            primary key,
    us_age       integer,
    us_cellphone varchar(255),
    us_direction varchar(255),
    us_email     varchar(255),
    us_name      varchar(255)
);

alter table tb_users
    owner to postgres;